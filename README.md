# Shared CI Pipeline Project

Ziel dieses Projektes ist gemeinsame Pipeline Elemente hier zu definieren und zu verteilen.
Diese beinhalten:

* wiederverwendbare Docker Images für die Build-Pipeline
* wiederverwendbare Pipeline Elemente (wie z.B. Security Checks von Docker Images)

## Docker Images

Folgende Docker Images stehen für den bau der diversen Applikation generisch zur Verfügung.

| Image Name                                                                        | Beschreibung                      |
|-----------------------------------------------------------------------------------|-----------------------------------|
| [registry.gitlab.com/adventisten/ci-cd-shared/node-18:1](images/node/Dockerfile)  | Node Version 18.x on Alpine       |
| [registry.gitlab.com/adventisten/ci-cd-shared/helm-3:1](images/helm/Dockerfile)   | Helm Version 3.11.0 on Alpine     |
| [registry.gitlab.com/adventisten/ci-cd-shared/util:1](images/util/Dockerfile)     | Linux utils based on Alpine       |


## Pipeline Snippets


## Pipeline Usage

#### Build and push image to GitLab

```yaml
include:
  - project: 'adventisten/ci-cd-shared'
    ref: main
    file: '/templates/docker-build.gitlab-ci.yml'

node-18:
  extends: .docker-build-template-gitlab
  stage: build-images
  variables:
    DOCKER_BUILD_TEMPLATE_IMAGE: $CI_REGISTRY_IMAGE/<image-name-and-tag>
    DOCKER_BUILD_TEMPLATE_DOCKERFILE: <path-to-Dockerfile>
    BUILD_ARG: <optional-additional-build-args>
```

| Variable name                  | Description and example value                                     |
|--------------------------------|-------------------------------------------------------------------|
| DOCKER_BUILD_SHARED_IMAGE_NAME | Name of the image incl. tag (e.g. `$CI_REGISTRY_IMAGE/node-18-1`) |
| DOCKER_BUILD_SHARED_DOCKERFILE | Location of the Dockerfile `images/node/Dockerfile`               |
| BUILD_ARG                      | Additional build arguments, e.g. `--build-arg NODE_VERSION=18`    |


#### Build and push image to external repository

```yaml
include:
  - project: 'adventisten/ci-cd-shared'
    ref: main
    file: '/templates/docker-build.gitlab-ci.yml'

node-14:
  extends: .docker-build-template
  stage: build-images
  variables:
    DOCKER_BUILD_TEMPLATE_REGISTRY: <registry name>
    DOCKER_BUILD_TEMPLATE_USER: <username>
    DOCKER_BUILD_TEMPLATE_TOKEN: <secret>
    DOCKER_BUILD_TEMPLATE_IMAGE: <image-name>
    DOCKER_BUILD_TEMPLATE_DOCKERFILE: <path-to-Dockerfile>
    BUILD_ARG: <optional-additional-build-args>
```

| Variable name                    | Description and example value                                         |
|----------------------------------|-----------------------------------------------------------------------|
| DOCKER_BUILD_TEMPLATE_REGISTRY   | Path to the registry, e.g. `hub.docker.com`                           |
| DOCKER_BUILD_TEMPLATE_USER       | Name to identify the access to the repository                         |
| DOCKER_BUILD_TEMPLATE_TOKEN      | Secret for the username to access the repository                      |
| DOCKER_BUILD_TEMPLATE_IMAGE      | Name of the image incl. tag (e.g. `node-14-1`)                        |
| DOCKER_BUILD_TEMPLATE_DOCKERFILE | Location of the Dockerfile `images/node/Dockerfile`                   |
| BUILD_ARG                        | Additional build arguments, e.g. `--build-arg NODE_VERSION=18`        |


### Helm

The template [`helm-build.gitlab-ci.yml`](/templates/helm-build.gitlab-ci.yml) helps to easily publish an artifact to the GitLab repository 

#### Build and push chart to GitLab

```yaml
include:
  - project: 'adventisten/ci-cd-shared'
    ref: main
    file: '/templates/helm-build.gitlab-ci.yml'

<helm-job>:
  extends: .helm-build-template-gitlab
  stage: helm
  before_script:
    - cd helm
```

| Variable name               | Description and example value        |
|-----------------------------|--------------------------------------|
| HELM_BUILD_TEMPLATE_VERSION | Version of the current helm template |


### Quality

#### Code Quality Checks

```yaml
# Include Code-Quality.gitlab-ci.yml template from https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml
include:
  - template: Code-Quality.gitlab-ci.yml

# If needed, define in which stage it should be performed, which artifacts are needed and how long the report should be kept
code_quality:
  stage: quality
  needs:
    - job: maven
      artifacts: true
  artifacts:
    paths: [gl-code-quality-report.json]
    expire_in: 4 weeks
```

#### Image Quality Checks

```yaml
# Include Container-Scanning.gitlab-ci.yml template from https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml
include:
  - template: Security/Container-Scanning.gitlab-ci.yml

container_scanning:
  stage: quality
  needs:
    - job: maven
      artifacts: true
  variables:
    GIT_STRATEGY: fetch
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE/$DOCKER_IMAGE_NAME:$BUILD_VERSION_NAME
    DOCKER_USER: $CI_REGISTRY_USER
    DOCKER_PASSWORD: $CI_REGISTRY_PASSWORD
```

or

```yaml
include:
  - project: 'adventisten/ci-cd-shared'
    ref: main
    file: '/templates/container-scan.gitlab-ci.yml'

container_scanning_trify:
  stage: quality
  needs:
    - docker-build-and-push-gitlab
    - maven
  extends: .container-scan-gitlab-template
  variables:
    CONTAINER_SCAN_TEMPLATE_IMAGE: $CI_REGISTRY_IMAGE/$DOCKER_IMAGE_NAME:$BUILD_VERSION_NAME
```
